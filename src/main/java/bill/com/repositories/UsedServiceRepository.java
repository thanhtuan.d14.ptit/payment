package bill.com.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import bill.com.model.UsedServiceModel;

public interface UsedServiceRepository extends JpaRepository<UsedServiceModel, Integer>{
	
	@Query(value="select *, (used_service.quantity*service.price_per_unit) AS total_price"
			+ " from used_service inner join service on used_service.service_id=service.id"
			+ " where contract_id= ?1 and bill_id=0", nativeQuery=true)
	public List<UsedServiceModel> listByContractId(Integer contractId);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO used_service (id, bill_id) VALUES(?1, ?2) ON DUPLICATE KEY UPDATE bill_id=?2",nativeQuery=true)
	public void updateBillId(Integer id, Integer billId);
}
