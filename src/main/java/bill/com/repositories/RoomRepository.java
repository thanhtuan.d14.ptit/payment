package bill.com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import bill.com.model.RoomModel;

public interface RoomRepository extends JpaRepository<RoomModel, Integer> {

}
