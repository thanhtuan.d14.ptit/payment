package bill.com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import bill.com.model.BillModel;

public interface BillRepository extends JpaRepository<BillModel, Integer> {
	
}
