package bill.com.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import bill.com.model.StudentModel;

public interface StudentRepository extends JpaRepository<StudentModel, Integer>{

	@Query(value="select student.* from room inner join contract on room.id = contract.room_id "
			+ "inner join student on contract.student_id = student.id where room_id = ?1",nativeQuery=true)
	public List<StudentModel> findByRoomId(Integer room);
}
