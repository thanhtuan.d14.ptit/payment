package bill.com.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import bill.com.model.ContractModel;

public interface ContractRepository extends JpaRepository<ContractModel, Integer>{
	
	@Query(value="select contract.* from room inner join contract on room.id = contract.room_id "
			+ "inner join student on contract.student_id = student.id "
			+ "where student_id=?1 and room_id=?2" ,nativeQuery=true)
	public ContractModel findByName(Integer student, Integer room);
}
