package bill.com.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bill.com.model.ResponseData;
import bill.com.services.RoomService;

@RestController
@RequestMapping("/room")
public class RoomController {

	@Autowired
	private RoomService roomService;
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ResponseData listAll(){
		return new ResponseData("SUCCESS", 200, roomService.listAll());
	}
}
