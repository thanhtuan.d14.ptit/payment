package bill.com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bill.com.model.ResponseData;
import bill.com.services.ContractService;

@RestController
@RequestMapping("/contract")
public class ContractController {
	
	@Autowired
	private ContractService contractService;
	
	@RequestMapping("/find")
	public ResponseData findByName(@RequestParam(value="student", required=false)Integer student, 
			@RequestParam(value="room",required=false)Integer room){
		return new ResponseData("SUCCESS", 200, contractService.findByName(student, room));
	}
}
