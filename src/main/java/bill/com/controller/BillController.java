package bill.com.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bill.com.model.BillModel;
import bill.com.model.ResponseData;
import bill.com.model.UsedServiceModel;
import bill.com.services.BillService;
import bill.com.services.UsedServiceService;

import org.springframework.web.bind.annotation.RequestMethod;


@RestController
@RequestMapping("/bill")
public class BillController {

	@Autowired
	private BillService billService;
	
	@Autowired
	private UsedServiceService usedService;
	
	@CrossOrigin(origins = "*") 
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public ResponseData saveBill(@RequestBody BillModel bill){
		if(bill.getContractId()==null) return new ResponseData("FAILURE", 400);
		Date createdDate = Date.valueOf(LocalDate.now());
		bill.setCreatedDate(createdDate);
		BillModel s = billService.saveBill(bill);
		List<UsedServiceModel> list = usedService.listAll(s.getContractId());
		for(UsedServiceModel l : list){
			if(l.getBillId()==0){
			usedService.updateBillId(l.getId(), s.getId());
			}
		}
		return new ResponseData("SUCCESS", 200);
	}
	
	@RequestMapping("/list")
	public List<BillModel> listAll(){
		return billService.listBill();
	}
}
