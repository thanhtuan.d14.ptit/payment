package bill.com.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bill.com.model.ResponseData;
import bill.com.services.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ResponseData listAll(@RequestParam(value="room",required=false)Integer room){
		return new ResponseData("SUCCESS", 200, studentService.listAll(room));
	}
}
