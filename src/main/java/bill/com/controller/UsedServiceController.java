package bill.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bill.com.model.ResponseData;
import bill.com.model.UsedServiceModel;
import bill.com.services.UsedServiceService;

@RestController
@RequestMapping("/usedservice")
public class UsedServiceController {
	
	@Autowired
	private UsedServiceService usedServiceService;
	
	@RequestMapping("/list")
	public ResponseData listAll(@RequestParam(value="contract",required=false)Integer contract){
		
		return  new ResponseData("SUCCESS", 200, usedServiceService.listAll(contract));
	}
}
