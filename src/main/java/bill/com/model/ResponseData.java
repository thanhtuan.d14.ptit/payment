package bill.com.model;

public class ResponseData {
	
	private String status;
	private Integer code;
	private Object content;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public Object getContent() {
		return content;
	}
	public void setContent(Object content) {
		this.content = content;
	}
	public ResponseData(String status, Integer code, Object content) {
		super();
		this.status = status;
		this.code = code;
		this.content = content;
	}
	public ResponseData(String status, Integer code) {
		super();
		this.status = status;
		this.code = code;
	}
	
}
