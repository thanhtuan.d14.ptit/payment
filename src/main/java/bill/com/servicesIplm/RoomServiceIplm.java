package bill.com.servicesIplm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bill.com.model.RoomModel;
import bill.com.repositories.RoomRepository;
import bill.com.services.RoomService;

@Service
public class RoomServiceIplm implements RoomService {

	@Autowired
	private RoomRepository roomRepository;
	
	@Override
	public List<RoomModel> listAll() {
		// TODO Auto-generated method stub
		return roomRepository.findAll();
	}

}
