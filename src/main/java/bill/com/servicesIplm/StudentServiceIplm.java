package bill.com.servicesIplm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bill.com.model.StudentModel;
import bill.com.repositories.StudentRepository;
import bill.com.services.StudentService;

@Service
public class StudentServiceIplm implements StudentService{

	@Autowired
	private StudentRepository studentRepository;

	@Override
	public List<StudentModel> listAll(Integer room) {
		
		// TODO Auto-generated method stub
		if(room==null) return studentRepository.findAll(); 
		return studentRepository.findByRoomId(room);
	}
}
