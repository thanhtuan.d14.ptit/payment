package bill.com.servicesIplm;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bill.com.model.ContractModel;
import bill.com.repositories.ContractRepository;
import bill.com.services.ContractService;

@Service
public class ContractServiceIplm implements ContractService{
	@Autowired
	private ContractRepository contractRepository;
	@Override
	public ContractModel findByName(Integer student, Integer room) {
		// TODO Auto-generated method stub
		return contractRepository.findByName(student, room);
	}
	
}
