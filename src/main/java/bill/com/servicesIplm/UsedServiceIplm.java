package bill.com.servicesIplm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bill.com.model.UsedServiceModel;
import bill.com.repositories.UsedServiceRepository;
import bill.com.services.UsedServiceService;

@Service
public class UsedServiceIplm implements UsedServiceService {
	
	@Autowired
	private UsedServiceRepository usedServiceRepository;
	
	@Override
	public List<UsedServiceModel> listAll(Integer contractId) {
		// TODO Auto-generated method stub
		return usedServiceRepository.listByContractId(contractId);
	}

	@Override
	public UsedServiceModel save(UsedServiceModel usedService) {
		// TODO Auto-generated method stub
		return usedServiceRepository.save(usedService);
	}

	@Override
	public void updateBillId(Integer id, Integer billId) {
		// TODO Auto-generated method stub
		usedServiceRepository.updateBillId(id, billId);
	}
	
}
