package bill.com.servicesIplm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bill.com.model.BillModel;
import bill.com.repositories.BillRepository;
import bill.com.services.BillService;

@Service
public class BillServiceIplm implements BillService {

	@Autowired
	private BillRepository billRepository;
	
	@Override
	public BillModel saveBill(BillModel bill) {
		// TODO Auto-generated method stub
		return billRepository.save(bill);
	}

	@Override
	public List<BillModel> listBill() {
		// TODO Auto-generated method stub
		return billRepository.findAll();
	}

}
