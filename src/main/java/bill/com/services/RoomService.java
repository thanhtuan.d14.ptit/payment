package bill.com.services;

import java.util.List;

import bill.com.model.RoomModel;

public interface RoomService {
	public List<RoomModel> listAll();
}
