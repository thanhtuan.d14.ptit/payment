package bill.com.services;


import bill.com.model.ContractModel;

public interface ContractService {
	public ContractModel findByName(Integer student, Integer room);
}
