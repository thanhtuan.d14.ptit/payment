package bill.com.services;

import java.util.List;

import bill.com.model.UsedServiceModel;

public interface UsedServiceService {
	
	public List<UsedServiceModel> listAll(Integer contractId);
	
	public UsedServiceModel save(UsedServiceModel usedSerive);
	
	public void updateBillId(Integer id, Integer billId);
}
