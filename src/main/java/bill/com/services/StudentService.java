package bill.com.services;

import java.util.List;

import bill.com.model.StudentModel;

public interface StudentService {
	public List<StudentModel> listAll(Integer room);
}
