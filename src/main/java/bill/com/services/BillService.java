package bill.com.services;

import java.util.List;

import bill.com.model.BillModel;

public interface BillService {

	public BillModel saveBill(BillModel bill);
	
	public List<BillModel> listBill();
}
